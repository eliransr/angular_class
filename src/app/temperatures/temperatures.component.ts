import { TempService } from './../temp.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

  likes:number = 0; 
  temperature:number;
  city:String;
  image:String;
  errorMessage:String;
  hasError:Boolean = false;


  tempData$:Observable<Weather>

  addLikes(){
    this.likes++
  }

  constructor(private router:ActivatedRoute , private tempservice:TempService) { }

    ngOnInit() {
      
      this.city = this.router.snapshot.params.city;
      // לא מעניין כי אנחנו הולכים לקרוא את זה עכשיו מהשרת
      // this.temperature = this.router.snapshot.params.temp;
      this.tempData$ = this.tempservice.searchWeatherData(this.city);
      this.tempData$.subscribe(
        data=> {
          console.log(data);
          this.temperature = data.temperature;
          this.image=data.image;
      },
      error=>{
        this.hasError = true;
        this.errorMessage = error.message; 
        console.log('in the component' + error.message);
      }
    )
  }
}