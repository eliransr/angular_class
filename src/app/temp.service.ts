import { Weather } from './interfaces/weather';
import { Injectable } from '@angular/core';
import { HttpClient , HttpErrorResponse} from '@angular/common/http';
import { Observable , throwError} from 'rxjs';
import { WeatherRaw } from './interfaces/weather-raw';
import { map ,catchError} from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class TempService {
 private URL = "http://api.openweathermap.org/data/2.5/weather?q=";
 private KEY = "bd985d54325197c58aa8b892de055079"
 private IMP = "&units=metric"


  searchWeatherData(cityName:String):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP} `)
    // function who translate from the WeatherRaw to Weather
    .pipe(map(data =>this.transformWeatherData(data)),
    catchError(this.hanndelError)
    )
  }

  // #שגיאות שמיועדת מחלקה-HttpErrorResponse
  private hanndelError(res:HttpErrorResponse)
  {
    console.log("in the service" + res.error);
    return throwError(res.error)
  }


  private transformWeatherData(data:WeatherRaw):Weather
  {
    return{
      name:data.name,
      country:data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description: data.weather[0].description, 
      temperature: data.main.temp,
      lon:data.coord.lon,
    
    }
  }

  constructor(private http:HttpClient) { }
}


