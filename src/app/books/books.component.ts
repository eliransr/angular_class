import { Observable } from 'rxjs';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service'; 

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  // books:any;
   books$:Observable<any>;
   userId:string;

  deleteBook(id){
    this.bookservice.deleteBook(this.userId,id)
    console.log(id);
  }
  
  constructor(private bookservice:BooksService,
              public authService:AuthService) { }

  ngOnInit() { 
    /*
    this.books = this.booksservice.getbooks(.subscrube(
      (books)=> this.books = books
    )
    */
   //this.booksservice.addBooks();
   //this.books$ = this.booksservice.getbooks();
    console.log("NgOnInit started")  
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.books$ = this.bookservice.getBooks(this.userId); 
      }
    )
  }
}
