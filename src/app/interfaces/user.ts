export interface User {
//    ? - mean Field isnt requried
    uid:string,
    email?: string | null,
    photoUrl?: string,
    displayName?:string
 
}
