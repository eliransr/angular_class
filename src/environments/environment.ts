// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD67KmtSPEV_F9HLIBF60kQA2MCyTOt4w8",
    authDomain: "bbbc-e8e66.firebaseapp.com",
    databaseURL: "https://bbbc-e8e66.firebaseio.com",
    projectId: "bbbc-e8e66",
    storageBucket: "bbbc-e8e66.appspot.com",
    messagingSenderId: "853682627980",
    appId: "1:853682627980:web:ab8d3ade941af63e96a2a9",
    measurementId: "G-HFEGZ3Q42N"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
